use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct FacturaElectronica {
    pub clave: String,
    pub codigo_actividad: String,
    pub numero_consecutivo: String,
    pub fecha_emision: String,
    pub emisor: Emisor,
    pub receptor: Receptor,
    pub condicion_venta: String,
    pub plazo_credito: Option<u32>,
    pub medio_pago: String,
    pub detalle_servicio: Vec<DetalleServicio>,
    pub resumen_factura: ResumenFactura,
    pub otros: Option<Otros>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Emisor {
    pub nombre: String,
    pub identificacion: Identificacion,
    pub nombre_comercial: Option<String>,
    pub ubicacion: Option<Ubicacion>,
    pub telefono: Option<Telefono>,
    pub correo_electronico: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Receptor {
    pub nombre: String,
    pub identificacion: Identificacion,
    pub nombre_comercial: Option<String>,
    pub ubicacion: Option<Ubicacion>,
    pub telefono: Option<Telefono>,
    pub correo_electronico: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Identificacion {
    pub tipo: String,
    pub numero: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Ubicacion {
    pub provincia: String,
    pub canton: String,
    pub distrito: String,
    pub barrio: Option<String>,
    pub otras_senas: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Telefono {
    pub codigo_pais: String,
    pub num_telefono: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Otros {
    pub otro_texto: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DetalleServicio {
    pub linea_detalle: Vec<LineaDetalle>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct LineaDetalle {
    pub numero_linea: String,
    pub codigo: String,
    pub codigo_comercial: CodigoComercial,
    pub cantidad: String,
    pub unidad_medida: String,
    pub unidad_medida_comercial: Option<String>,
    pub detalle: String,
    pub precio_unitario: String,
    pub monto_total: String,
    pub sub_total: String,
    pub monto_total_linea: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct CodigoComercial {
    pub tipo: String,
    pub codigo: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct ResumenFactura {
    pub codigo_tipo_moneda: TipoMoneda,

    #[serde(default = "default_total")]
    pub total_serv_gravados: f32,
    #[serde(default = "default_total")]
    pub total_serv_exentos: f32,
    #[serde(default = "default_total")]
    pub total_serv_exonerado: f32,

    #[serde(default = "default_total")]
    pub total_mercancias_gravadas: f32,
    #[serde(default = "default_total")]
    pub total_mercancias_exentas: f32,
    #[serde(default = "default_total")]
    pub total_merc_exonerada: f32,

    pub total_gravado: f32,
    #[serde(default = "default_total")]
    pub total_exento: f32,
    #[serde(default = "default_total")]
    pub total_exonerado: f32,

    pub total_venta: f32,
    #[serde(default = "default_total")]
    pub total_descuentos: f32,
    pub total_venta_neta: f32,
    pub total_impuesto: f32,
    pub total_comprobante: f32,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct TipoMoneda {
    pub codigo_moneda: String,
    pub tipo_cambio: String,
}

fn default_total() -> f32 {
    return 0.0;
}

pub fn parse_fact(xml_file: &std::path::PathBuf) -> FacturaElectronica {
    let xml = std::fs::read_to_string(xml_file).unwrap();
    return quick_xml::de::from_str(&xml).unwrap();
}
